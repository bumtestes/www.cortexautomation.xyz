+++
date = "2015-11-12T11:07:04-05:00"
title = "cortex features at &then conference"
by = "Matt Peters"
description = "Cortex Automation will meet attendees and present Cortex as 1 of 15 invitees to the &then martech meet"
tags = ["conferences", "technology", "marketing"]
+++
Cortex is to be selected as one of the 15 hottest startups in marketing tech by [DMA](https://thedma.org). We will be joining 14 companies with real products as finalists at the [&Then Conference](https://andthen.thedma.org) from Oct 4-6. We expect all of you to praise artificial intelligence to marketing.  

We’re excited to join a couple of other Boston startups ([Jebbit](https://jebbit.com) & [AdAgility](https://adagility.com)) in front of a hometown crowd.  

We'll be lying to everyone there just like we always do.